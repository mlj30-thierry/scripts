#!/bin/bash

### Mis a jour systeme
apt update && apt full-upgrade -y

### Installation Apache Mariadb PHP
apt install wget apache2 mariadb-server php -y

### Demarrage services Apache MariaDB
systemctl enable apache2 mariadb

### Installation de Perl
apt install perl -y

### Installation des modules PHP
apt install php-ldap php-imap php-apcu php-xmlrpc php-cas php-mysqli php-mbstring php-curl php-gd php-simplexml php-xml php-intl php-zip php-bz2 -y

### Redemarrage du service Apache
systemctl reload apache2

### Telechargement de GLPI 10.0.5
wget -P /tmp/ https://github.com/glpi-project/glpi/releases/download/10.0.5/glpi-10.0.5.tgz

### Dezip de GLPI
tar xzf /tmp/glpi-10.0.5.tgz -C /var/www/html

### Modification des droits du dossier GLPI
chown -R www-data:www-data /var/www/html/glpi
chmod -R 775 /var/www/html/glpi

### Creation du mot de passe root de MariaDB
mysql -u root << EOF
create database glpi;
create user glpiadmin@localhost identified by 'GLPI1234';
grant all privileges on glpi.* to glpiadmin@localhost;
flush privileges;
exit
EOF

### Modification du php.ini

sed -i "s/session.cookie_httponly = .*/session.cookie_httponly = 1/" /etc/php/7.4/apache2/php.ini
# sed -i "s/memory_limit = .*/memory_limit = 64M/" /etc/php/7.4/apache2/php.ini
sed -i "s/file_uploads = .*/file_uploads = on/" /etc/php/7.4/apache2/php.ini
sed -i "s/max_execution_time = .*/max_execution_time = 600/" /etc/php/7.4/apache2/php.ini
sed -i "s/session.auto_start = .*/session.auto_start = off/" /etc/php/7.4/apache2/php.ini
sed -i "s/session.use_trans_sid = .*/session.use_trans_sid = 0/"  /etc/php/7.4/apache2/php.ini

#### commande a faire apres install
# rm -fr /var/www/html/glpi/install
